# Energy Data API documentation
## Units
If the column name starts with a "p" its value is expressed in the unit Bar.
If the column name starts with a "t" (excluding timeofmetric) its value is expressed in the unit celsius
## Data format
The data is formated as JSON objects. The data in question has been generated every 15 min from the year 2013 to 2016.
The data is formated in the following way :
* id
Unique ID for every set of values (Integer)
* timeofmetric
Timestamp from the time the messurements where taken.(DateTime)
* pFramled****
Value of the heating pressure going out to the location. (Decimal)
* tFramled****
Value of the temprature of the heating on its way to the location.(Decimal)
* pRetur****
Value representing the heating pressure returning from the destinated area.(Decimal)
* tRetur****
Value of the temprature the water has when its returning.(Decimal)
## Other open API:s
- [Kolada API](https://www.kolada.se/?_p=index/API)
- [oppnadata.se](https://oppnadata.se/)
- [Västerås psi-data](https://www.vasteras.se/mall-sidor/psi-data.html)
- [Any API](https://any-api.com/)
## API links
To access the JSON data for different locations use these links.

1. Skultuna
* [Skultuna 2013-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2013/1)
* [Skultuna 2013-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2013/2)
* [Skultuna 2014-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2014/1)
* [Skultuna 2014-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2014/2)
* [Skultuna 2015-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2015/1)
* [Skultuna 2015-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2015/2)
* [Skultuna 2016-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2016/1)
* [Skultuna 2016-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/skultuna/2016/2)
2. Barkarö
* [Barkarö 2013-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2013/1)
* [Barkarö 2013-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2013/2)
* [Barkarö 2014-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2014/1)
* [Barkarö 2014-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2014/2)
* [Barkarö 2015-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2015/1)
* [Barkarö 2015-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2015/2)
* [Barkarö 2016-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2016/1)
* [Barkarö 2016-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/barkaro/2016/2)
3. Ekbergaskolan
* [Ekbergaskolan 2013-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2013/1)
* [Ekbergaskolan 2013-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2013/2)
* [Ekbergaskolan 2014-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2014/1)
* [Ekbergaskolan 2014-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2014/2)
* [Ekbergaskolan 2015-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2015/1)
* [Ekbergaskolan 2015-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2015/2)
* [Ekbergaskolan 2016-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2016/1)
* [Ekbergaskolan 2016-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/ekberga/2016/2)
4. Trollbackskolan
* [Trollbackskolan 2013-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2013/1)
* [Trollbackskolan 2013-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2013/2)
* [Trollbackskolan 2014-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2014/1)
* [Trollbackskolan 2014-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2014/2)
* [Trollbackskolan 2015-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2015/1)
* [Trollbackskolan 2015-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2015/2)
* [Trollbackskolan 2016-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2016/1)
* [Trollbackskolan 2016-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/trollback/2016/2)
5. Håkantorpsskolan
* [Håkantorpsskolan 2013-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2013/1)
* [Håkantorpsskolan 2013-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2013/2)
* [Håkantorpsskolan 2014-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2014/1)
* [Håkantorpsskolan 2014-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2014/2)
* [Håkantorpsskolan 2015-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2015/1)
* [Håkantorpsskolan 2015-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2015/2)
* [Håkantorpsskolan 2016-1](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2016/1)
* [Håkantorpsskolan 2016-2](https://04m8q6i6g2.execute-api.eu-central-1.amazonaws.com/dev/hakantorp/2016/2)
6. Hökåsenskolan
* [Hökåsenskolan 2013-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2013/1)
* [Hökåsenskolan 2013-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2013/2)
* [Hökåsenskolan 2014-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2014/1)
* [Hökåsenskolan 2014-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2014/2)
* [Hökåsenskolan 2015-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2015/1)
* [Hökåsenskolan 2015-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2015/2)
* [Hökåsenskolan 2016-1](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2016/1)
* [Hökåsenskolan 2016-2](https://44gm5pbsl2.execute-api.eu-central-1.amazonaws.com/dev/hokasen/2016/2)