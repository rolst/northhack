# NorthHack

- [Introduction & Disclaimers](###Introduction-&-disclaimers)
- [Setup](###Setup)
- [AWS](###AWS)
- [Data & API](./apis.md)

### Introduction & Disclaimers
Welcome to Northhack! This hackathon will focus on how to use and analyze open data.

We have real data from Mälarenergi and some examples of other open data API's.

Each team will have access to AWS resources through an account, on first login you will be asked to set a new password.

To connect to the AWS management console you need to login. You login using a sign-on page which is unique to each team.

The accounts that are provided will have access to the resources you need but they have som restrictions setup for security reasons.

If you run into any trouble please don't hesitate to ask the staff or join our slack channel: [Slack Invite]

---
- Username : nh-user
- Password : NorthHack2018
---
* Orange
[Team Orange](https://northhack-team-orange.signin.aws.amazon.com/console)
* Blue
[Team Blue](https://northhack-team-blue.signin.aws.amazon.com/console)
* Green
[Team Green](https://northhack-team-green.signin.aws.amazon.com/console)
* Teal
[Team Teal](https://northhack-team-teal.signin.aws.amazon.com/console)
* Sage
[Team Sage](https://northhack-team-sage.signin.aws.amazon.com/console)
* Purple
[Team Purple](https://northhack-team-purple.signin.aws.amazon.com/console)
* Brown
[Team Brown](https://northhack-team-brown.signin.aws.amazon.com/console)

---
### Setup
---

0. Login to AWS console and create an IAM user, add a user and add it to the `nh-admin`-group to allow it to use AWS resources.
Make sure you check the checkbox for `programmatic access` to be able to use AWS from the command line.

1. Install a code editor, we recommend [Visual Code]

2. Install Python runtime [Python Homepage]

3. Install AWS CLI
```
$ pip install --user awscli
```
4. Then follow the [AWS CLI configuration guide].

5. Setup node.js runtime, you can download it from https://nodejs.org or use `nvm` to install it.
    - [nvm](https://github.com/creationix/nvm)
    - [nvm for windows](https://github.com/coreybutler/nvm-windows)

    ```
    # run this to install nodejs v10.13 (latest stable release)
    $ nvm install 10.13
    ```


## AWS

Down below we've provided some examples how to use AWS for development.

Examples and instructions includes simple storage and hosting through S3, instructions for setting up an Relational Database, deploying a server with elasticbeanstalk and using serverless.

---
### S3
---
S3 is a Simple storage service, its basically a flat file storage, which means images, videos, static html webpages and similar objects. If you need to store a file system like an OS or datebase, use the EBS service available in the AWS management console.

#### What to use S3 for
Use S3 as a cheap way of storing objects, log files and txt files in general, you can make a "bucket" (folder) publically available, which means you can host webpages directly from your bucket. You can also set permissions on individuall objects.

#### Hosting a static website
* Navigate to S3 in the AWS Console.
* Click into your bucket.
* Click the “Properties” section.
* Click the “Static website hosting” option.
* Select “Use this bucket to host a website”.
* Enter “index.html” as the Index document.


## Documentation

[Getting Started with S3](https://docs.aws.amazon.com/AmazonS3/latest/gsg/s3-gsg.pdf)
[Whitepapers](https://aws.amazon.com/whitepapers/storage-options-aws-cloud/)

---
### Sumerian
AWS sumerian is web tool for building application with 3D animation in the browser.
- [Documentation](https://aws.amazon.com/sumerian/)
- [Västerås Example](https://5e11110359ee4b6cb13668cd67184a69.eu-west-1.sumerian.aws/)

---

### RDS
---
Use the management console to access the RDS service.
When creating an instance, you can choose what type of instance you want to create. We recommend for this hackathon to use the T2 series of instances.
[Using the management console](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Tutorials.WebServerDB.CreateDBInstance.html)

### Connect to an RDS
When you create an RDS you will create a root account. This can then be used to create more RDS accounts. To access your RDS you will need to enter the following
* Hostname
This is the endpoint address you recieve when the rds has started.
* Username
The username you used when creating the RDS
* Password
* Port
The port you use to connect to the database, as an example, MYSQL uses 3306

To connect to the actual database, you can either use the console or your favorite terminal application. Putty is a good one.

## NodeJS Code example of a connection config
```javascript
const dbConfig = {
  host: '*your endpoint address*',
  user: '******',
  password: '******',
  database: '*Your database name*',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
};

const dbPool = mysql.createPool(dbConfig)
```

---
### Elasticbeanstalk

Elasticbeanstalk is allows you to deploy your application in an environment, currently you can choose from a wide variety of languages.

To get started with Elasticbeanstalk, find a language of your preference through the side-menu (working with language X) and follow the getting started guide. [Elasticbeanstalk Get Started]

---
### Serverless
Serverless is a framework for working with various FAAS providers, our provider will be AWS.

It allows developers to easily use services such as `Lambda` and `API Gateway`.

You will need to have configured AWS CLI for this to work with your account.

[Install serverless](https://serverless.com/framework/docs/getting-started/)
[Getting started with AWS and Serverless](https://serverless.com/framework/docs/getting-started/)


---
### Data & API

[Data & API](./apis.md)

---


[Elasticbeanstalk Get Started]:
https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/Welcome.html

[Python Homepage]:
https://www.python.org/downloads/

[Visual Code]:
https://code.visualstudio.com/

[AWS CLI configuration guide]:
https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-quick-configuration

[Slack Invite]:
https://northhack.slack.com/join/shared_invite/enQtNDcwNjMwNzkxNDQ0LTJkYzViOTc2ZTg2ZDFkZWJjNjU4NjAxYzBhYzliOTg5NGU0YmFlYWYxMGYxYzMwYjU5MjQ5MzQxMmI1ZThkMDQ